#include <iostream>
#include <map>

int main(){
	std::map<int, std::string> StoreInventory;
	StoreInventory.emplace(1000, "Tea");
	StoreInventory.emplace(500, "Burger");
	StoreInventory.emplace(1500, "Soda");
	StoreInventory.emplace(100, "Water");
	StoreInventory.emplace(100000, "PC");
	
	int price; //Создаём переменную для приёма индекса товара

	std::cin >> price;//принимаем индекс товара

	auto Name = StoreInventory.find(price);//Ищем товар в бинарном дереве

	//Если товар есть, то выводим его имя. Иначе пишем что продукт не найден
	if (Name != StoreInventory.end()) {
	std::cout << Name->second << std::endl;
	}
	else
	{
	std::cout << "The product not found" << std::endl;
	}

}